Source: libextutils-parsexs-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>,
           Ansgar Burchardt <ansgar@debian.org>,
           gregor herrmann <gregoa@debian.org>,
           Florian Schlichting <fsfs@debian.org>,
           Dominic Hargreaves <dom@earth.li>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libextutils-parsexs-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libextutils-parsexs-perl.git
Homepage: https://metacpan.org/release/ExtUtils-ParseXS

Package: libextutils-parsexs-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         perl
Description: utility to process Perl XS code into C code
 ExtUtils::ParseXS is a Perl utility that provides a modular way of compiling
 Perl XS code into appropriate C code by embedding the constructs necessary to
 let C functions manipulate Perl values and creating the glue necessary to let
 Perl access those functions.
 .
 This module is already included as part of Perl's core distribution, so this
 package is only beneficial when newer features or bug fixes are required.
